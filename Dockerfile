FROM ros:noetic

WORKDIR /root/catkin_ws

# Setup enviroment
RUN echo "source /opt/ros/noetic/setup.sh" >> ~/.bashrc
RUN echo "source /root/catkin_ws/devel/setup.bash" >> ~/.bashrc
RUN echo "source /opt/ros/noetic/setup.sh" >> ~/.profile
RUN echo "source /root/catkin_ws/devel/setup.bash" >> ~/.profile

# install ros package
RUN apt-get update && apt-get install -y \
    ros-${ROS_DISTRO}-ros-tutorials \
    ros-${ROS_DISTRO}-common-tutorials && \
    rm -rf /var/lib/apt/lists/*

#RUN apt-get update && apt-get install -y python3.9 python3.9-dev
RUN apt-get update && apt-get install python3-pip git -y

# install packages
RUN apt-get update && apt-get install -y
RUN apt-get install python3-dev python3-rpi.gpio -y

# Standard Python Libraries
RUN pip3 install python-dotenv requests typing RPi.GPIO gpiozero geographiclib numpy pandas
RUN pip3 install --upgrade --force-reinstall adafruit-blinka Adafruit-PlatformDetect


# (10DOF Module) Library for L3GD20H Gyroscope
# https://learn.adafruit.com/adafruit-triple-axis-gyro-breakout/python-circuitpython
# https://github.com/adafruit/Adafruit_CircuitPython_L3GD20
RUN pip3 install adafruit-circuitpython-l3gd20

# (10DOF Module) Library for LSM303DLHC Accelerometer + Magnetfeld Sensor
# https://learn.adafruit.com/lsm303-accelerometer-slash-compass-breakout/python-circuitpython
RUN pip3 install adafruit-circuitpython-lsm303-accel adafruit-circuitpython-lsm303dlh-mag

# (10DOF Module) Library for BMP180 Drucksensor
# https://learn.adafruit.com/bmp085/downloads
# https://www.adafruit.com/product/1603
# https://github.com/adafruit/Adafruit_Python_BMP
RUN pip3 install git+https://github.com/adafruit/Adafruit_Python_BMP

# (GPS Module) Library for Venus638FLPx SparkFun Venus GPS with SMA Connector
# https://www.sparkfun.com/products/retired/11058
# https://www.skytraq.com.tw/AN0018_v3.7.pdf
# http://pabigot.github.io/bsp430/ex_sensors_venus6pps.html
# http://cdn.sparkfun.com/datasheets/Sensors/GPS/Venus/638/doc/Venus638FLPx_DS_v07.pdf
# Binary Messages of SkyTraq Venus 6 GPS Receiver
# http://dlnmh9ip6v2uc.cloudfront.net/datasheets/Sensors/GPS/Venus/638/doc/AN0003_v1.4.19.pdf
RUN pip3 install pynmea2

# Mount all needed UART interfaces
# Bind physical UART0 to Docker UART0 and its symlink serial0
VOLUME /dev/ttyAMA0 /dev/ttyAMA0
VOLUME /dev/serial0 /dev/serial0
# Bind physical I2C-1 to Docker I2C-1. I2C-[1-6] possible
# IC2-[2-6] need activation inside the config.txt
VOLUME /dev/i2c-1

# Copy the devel dir and the .catkin_workspace file. Just needed inside the container
COPY catkin_ws .

# Build the project inside the container
RUN /bin/bash -c '. /opt/ros/noetic/setup.bash; catkin_make -C /root/catkin_ws'

# Lunch all nodes
RUN /bin/bash -c '. /opt/ros/noetic/setup.bash;. devel/setup.sh; rospack find bsslaunch'
CMD /bin/bash -c '. /opt/ros/noetic/setup.bash;. devel/setup.sh; roslaunch --log bsslaunch bss.launch'

# # Execute the roscore
# CMD roscore

# default entryporint for keep on running without getting terminated 
# ENTRYPOINT ["tail", "-f", "/dev/null"]