const deleter = (ids) => {
    const promisesArr = []
    var myHeaders = {"apiKey":"haw-team5-d410f8dadb82", "Content-Type": "application/geo+json", "accept": "*/*" }
    var requestOptions = {  method: 'DELETE', headers: myHeaders, redirect: 'follow' };

    ids.map(id => {
        const pr = fetch(`https://eddy01.comloc.net/oaf/collections/traffic_signs/items/${id}?user=haw-team5-d410f8dadb82`, requestOptions)
            .catch(error => console.log('error', error));
        promisesArr.push(pr)
    })

    promises
        .all(promisesArr)
        .then(() => console.log("Done"))
        .catch(console.error)
}

var resIds
var requestOptions = { method: 'GET', redirect: 'follow' };

fetch("https://eddy01.comloc.net/oaf/collections/traffic_signs/items?apiKey=haw-team5-d410f8dadb82&createdBy=haw-team5&limit=1000", requestOptions)
    .then(response => response.json())
    .then(data => {
        resIds = data.features.map(feature => feature.id)  
    })
    .catch(error => console.log('error', error));


deleter(resIds)