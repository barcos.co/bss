#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import uuid
import rospy

from random import uniform
from bssmessage.msg import BSSPoint


nodeName = "bssexamplepublisher"
printNodeName = "[{}]".format(nodeName)
topicName = "bssexample/data"


def mainNode():

    pub = rospy.Publisher(topicName, BSSPoint, queue_size=10)
    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    # publishes at a rate of 2 messages per second
    rate = rospy.Rate(2)  # 2hz

    while not rospy.is_shutdown():
        # message = "Hello world %s" % rospy.get_time()

        msg = BSSPoint()
        lat, long = uniform(-180, 180), uniform(-90, 90)

        msg.id = uuid.uuid4().hex
        msg.timestamp = rospy.get_rostime()
        msg.hasValue = True
        msg.latitude = lat
        msg.longitude = long

        rospy.loginfo("{} BSSPoint will be published".format(printNodeName))
        rospy.loginfo("{} BSSPoint id={} time={} latitude={} longitude={} hasValue={}".format(
            printNodeName, msg.id, msg.timestamp, msg.latitude, msg.longitude, msg.hasValue))
        rospy.loginfo(
            "{} BSSPoint id={} published successfully\n".format(printNodeName, msg.id))

        pub.publish(msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        mainNode()
    except rospy.ROSInterruptException:
        pass

# Dont forget to give execution rights -> chmod +x publisher.py
# Execurte by running -> python3 publisher.py
# Listen the Topic by running rostopic echo /bssexample/data in Terminal
# Better ->> Start with rosrun -> rosrun bssexample publisher.py
