#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import rospy
from bssmessage.msg import BSSPoint

topicName = "bssexample/data"
nodeName = "bssexamplesubscriber"
printNodeName = "[{}]".format(nodeName)


def callback(data: BSSPoint):
    # rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    rospy.loginfo("{} BSSPoint arrived id={} time={} latitude={} longitude={} hasValue={} speed={} course={}".format(
        printNodeName, data.id, data.timestamp, data.latitude, data.longitude, data.hasValue, data.speed, data.course))


def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    rospy.Subscriber(topicName, BSSPoint, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    try:
        listener()
    except rospy.ROSInterruptException:
        pass

# Dont forget to give execution rights -> chmod +x subscriber.py
# Execurte by running -> python3 subscriber.py
# Better ->> Start with rosrun -> rosrun bssexample subscriber.py
