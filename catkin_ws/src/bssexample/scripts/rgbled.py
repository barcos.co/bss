#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import uuid
import rospy

from random import uniform
from random import randint
from bssmessage.msg import BSSRGBLedPayload


nodeName = "bssactuatorsrgbledpublisher"
printNodeName = "[{}]".format(nodeName)
topicName = "bssactuators/rgbled"


def mainNode():

    pub = rospy.Publisher(topicName, BSSRGBLedPayload, queue_size=10)
    rospy.init_node(nodeName, anonymous=True)
    while not rospy.is_shutdown():

        msg = BSSRGBLedPayload()

        msg.id = uuid.uuid4().hex
        msg.timestamp = rospy.get_rostime()

        # msg.state = "RUNNING"

        # color
        # msg.color = "blue"

        msg.red = randint(0, 255)
        msg.green = randint(0, 255)
        msg.blue = randint(0, 255)

        # Pink RGB
        # msg.red = 255
        # msg.green = 20
        # msg.blue = 147

        # One of "RUNNING", "PARKED", "ALARM", "INTERNAL_ERROR"

        rospy.loginfo(
            "{} BSSRGBLedPayload will be published".format(printNodeName))
        rospy.loginfo("{} BSSRGBLedPayload id={} time={} state={}".format(
            printNodeName, msg.id, msg.timestamp, msg.state))
        pub.publish(msg)
        rospy.loginfo(
            "{} BSSRGBLedPayload id={} published successfully\n".format(printNodeName, msg.id))

        rospy.sleep(0.5)


if __name__ == '__main__':
    try:
        mainNode()
    except rospy.ROSInterruptException:
        pass
