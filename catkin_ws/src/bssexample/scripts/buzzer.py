#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import uuid
import rospy

from random import uniform
from bssmessage.msg import BSSBuzzerPayload


nodeName = "bssactuatorsbuzzerpublisher"
printNodeName = "[{}]".format(nodeName)
topicName = "bssactuators/buzzer"


def mainNode():

    pub = rospy.Publisher(topicName, BSSBuzzerPayload, queue_size=10)
    rospy.init_node(nodeName, anonymous=True)
    while not rospy.is_shutdown():

        msg = BSSBuzzerPayload()

        msg.id = uuid.uuid4().hex
        msg.timestamp = rospy.get_rostime()
        # 880 is max and 220 is min
        msg.frequency = 880.0
        msg.duration = 1.0

        # # One of "START", "ALARM", "NET_CONN", "NET_DISC", "INTERNAL_ERROR"
        # msg.melody

        rospy.loginfo(
            "{} BSSBuzzerPayload will be published".format(printNodeName))
        rospy.loginfo("{} BSSBuzzerPayload id={} time={} frequency={}".format(
            printNodeName, msg.id, msg.timestamp, msg.frequency))
        pub.publish(msg)
        rospy.loginfo(
            "{} BSSBuzzerPayload id={} published successfully\n".format(printNodeName, msg.id))

        rospy.sleep(5)


if __name__ == '__main__':
    try:
        mainNode()
    except rospy.ROSInterruptException:
        pass
