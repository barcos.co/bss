#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import uuid
import rospy

from random import uniform
from src.L3DG20H import Gyroscope
from bssmessage.msg import BSSIMUPayload
from src.LSM303DLHC import Accelerometer, Magnetometer
from src.BMP180 import PressureAndTemperature


nodeName = "bsssensorsimu"
printNodeName = "[{}]".format(nodeName)
topicName = "bssdata/imu"

gyro = Gyroscope(nodeName=nodeName)
magno = Magnetometer(nodeName=nodeName)
accel = Accelerometer(nodeName=nodeName)
bmp = PressureAndTemperature(nodeName=nodeName)


def mainNode():
    # use a bigger queue_size than the rospy.Rate in order to prevent data loss
    pub = rospy.Publisher(topicName, BSSIMUPayload, queue_size=10)
    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    # publishes at a rate of 2 messages per second
    rate = rospy.Rate(2)  # 2hz

    while not rospy.is_shutdown():

        msg = BSSIMUPayload()

        msg.id = uuid.uuid4().hex
        msg.timestamp = rospy.get_rostime()
        mData = magno.getData()
        gData = gyro.getData()
        aData = accel.getData()
        bmpData = bmp.getData()

        if mData:
            msg.Mx = mData.Mx
            msg.My = mData.My
            msg.Mz = mData.Mz

        if gData:
            msg.Wx = gData.Wx
            msg.Wy = gData.Wy
            msg.Wz = gData.Wz

        if gData:
            msg.Ax = aData.Ax
            msg.Ay = aData.Ay
            msg.Az = aData.Az

        if bmpData:
            msg.altitude = bmpData.altitude
            msg.pressure = bmpData.pressure
            msg.temperature = bmpData.temperature

        rospy.loginfo(
            "{} BSSIMUPayload will be published".format(printNodeName))
        rospy.loginfo("{} BSSIMUPayload id={} time={}".format(
            printNodeName, msg.id, msg.timestamp))
        rospy.loginfo(
            "{} BSSIMUPayload id={} published successfully\n".format(printNodeName, msg.id))

        pub.publish(msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        mainNode()
    except rospy.ROSInterruptException:
        pass
