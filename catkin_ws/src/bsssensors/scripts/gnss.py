#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import uuid
import rospy

from random import uniform
from bssmessage.msg import BSSGNSSPayload
from src.Venus638FLPx import GPS, Point


nodeName = "bsssensorsgnss"
printNodeName = "[{}]".format(nodeName)
topicName = "bssdata/gnss"


def callback(pub, point: Point, speed: float, course: float):
    msg = BSSGNSSPayload()

    msg.id = uuid.uuid4().hex
    msg.timestamp = rospy.get_rostime()
    msg.hasValue = point.hasValue
    msg.latitude = point.latitude
    msg.longitude = point.longitude
    msg.speed = speed
    msg.course = course

    rospy.loginfo(
        "{} BSSGNSSPayload will be published to {}".format(printNodeName, topicName))
    rospy.loginfo("{} BSSGNSSPayload id={} time={} latitude={} longitude={} speed={} hasValue={}".format(
        printNodeName, msg.id, msg.timestamp, msg.latitude, msg.longitude, speed, msg.hasValue))

    pub.publish(msg)
    rospy.loginfo(
        "{} BSSGNSSPayload id={} published successfully\n".format(printNodeName, msg.id))


def mainNode():
    # use a bigger queue_size than the rospy.Rate in order to prevent data loss
    pub = rospy.Publisher(topicName, BSSGNSSPayload, queue_size=10)
    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    gps = GPS(pub, printNodeName)
    gps.subscribe(callback)

    while not rospy.is_shutdown():
        try:
            gps.trackPosition()
        except KeyboardInterrupt:
            pass
        except Exception as e:
            rospy.logerr("{} Exception :: {}".format(printNodeName, e))
            continue


if __name__ == '__main__':
    try:
        mainNode()
    except rospy.ROSInterruptException:
        pass

# Dont forget to give execution rights -> chmod +x publisher.py
# Execurte by running -> python3 publisher.py
# Listen the Topic by running rostopic echo /bssexample/data in Terminal
# Better ->> Start with rosrun -> rosrun bssexample publisher.py
