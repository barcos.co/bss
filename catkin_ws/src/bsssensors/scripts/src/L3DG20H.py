##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import board
import rospy
import adafruit_l3gd20

from typing import Union
from dataclasses import dataclass
from adafruit_l3gd20 import L3DS20_RANGE_250DPS, L3DS20_RATE_100HZ


@dataclass
class GyroscopeData:
    Wx: float = 0.0
    """ Dreahrate in x in rad/s """
    Wy: float = 0.0
    """ Dreahrate in y in rad/s """
    Wz: float = 0.0
    """ Dreahrate in z in rad/s """


class Gyroscope:
    name = "L3DG20H"
    sensor = None
    didStart = False
    nodeName = ""

    def __init__(self, nodeName=""):
        self.nodeName = nodeName
        try:
            i2c = board.I2C()
            self.sensor = adafruit_l3gd20.L3GD20_I2C(
                i2c, rng=L3DS20_RANGE_250DPS, rate=L3DS20_RATE_100HZ)
            self.didStart = True
        except Exception as err:
            self.didStart = False
            rospy.logerr('{} {} Exception: {}'.format(
                self.nodeName, self.name,  err))

    def didStart(self):
        return self.didStart

    def getData(self) -> Union[GyroscopeData, None]:
        if (self.didStart == False):
            rospy.logerr('{} {} I2C not configured'.format(
                self.nodeName, self.name))
        else:
            current = self.sensor.gyro
            return GyroscopeData(current[0], current[1], current[2])
