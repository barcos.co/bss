##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Dennis Hoffman
# Dennis.Hofmann@haw-hamburg.de
##

import time
import RPi.GPIO as GPIO

from typing import Union
from dataclasses import dataclass


@dataclass
class UltrasonicData:
    distance1: int = 0
    """ Abstand sensor 1 in mm """
    distance2: int = 0
    """ Abstand sensor 2 in mm """


class Ultrasonic:
    name = "HC-SR04"
    didStart = False

    # Ultraschall Sensor Konfiguration
    US1_SENSOR_TRIGGER = 23
    US1_SENSOR_ECHO = 24
    US2_SENSOR_TRIGGER = 27
    US2_SENSOR_ECHO = 22
    Messung_Max = 1                 # in sekunden
    Messung_Trigger = 0.00001       # in sekunden
    Messung_Pause = 1             # in sekunden
    Messung_Faktor = (343460 / 2)   # Schallgeschwindigkeit
    Abstand_Max = 4000              # Max value in mm
    Abstand_Max_Error = Abstand_Max + 1

    def __init__(self):
        try:
            self.configure()
            self.didStart = True
        except Exception as err:
            self.didStart = False
            print(str(err))

    def configure(self):
        # Ultraschall Sensor Initialisierung GPIO-Pins
        GPIO.setmode(GPIO.BCM)                    # GPIO Modus (BOARD / BCM)
        # Trigger-Pin = Raspberry Pi Output
        GPIO.setup(self.US1_SENSOR_TRIGGER, GPIO.OUT)
        # Echo-Pin = raspberry Pi Input
        GPIO.setup(self.US1_SENSOR_ECHO, GPIO.IN)
        # Trigger-Pin = Raspberry Pi Output
        GPIO.setup(self.US2_SENSOR_TRIGGER, GPIO.OUT)
        # Echo-Pin = raspberry Pi Input
        GPIO.setup(self.US2_SENSOR_ECHO, GPIO.IN)

    def US_SENSOR1_GetDistance(self):

        GPIO.output(self.US1_SENSOR_TRIGGER, True)
        time.sleep(self.Messung_Trigger)
        GPIO.output(self.US1_SENSOR_TRIGGER, False)

        # speichere Startzeit
        StartZeit1 = time.time()
        MaxZeit1 = StartZeit1 + self.Messung_Max
        # warte aus steigende Flanke von ECHO
        while StartZeit1 < MaxZeit1 and GPIO.input(self.US1_SENSOR_ECHO) == 0:
            StartZeit1 = time.time()

        # speichere Stopzeit
        StopZeit1 = StartZeit1
        # warte aus fallende Flanke von ECHO
        while StopZeit1 < MaxZeit1 and GPIO.input(self.US1_SENSOR_ECHO) == 1:
            StopZeit1 = time.time()

        if StopZeit1 < MaxZeit1:
            # berechne Zeitdifferenz zwischen Start und Ankunft in Sekunden
            Zeit1 = StopZeit1 - StartZeit1
            # berechne Distanz
            Distanz1 = Zeit1 * self.Messung_Faktor
        else:
            # setze Fehlerwert
            Distanz1 = self.Abstand_Max_Error

        # Distanz als Ganzzahl zurückgeben
        return int(Distanz1)

    def US_SENSOR2_GetDistance(self):

        GPIO.output(self.US2_SENSOR_TRIGGER, True)
        time.sleep(self.Messung_Trigger)
        GPIO.output(self.US2_SENSOR_TRIGGER, False)

        # speichere Startzeit
        StartZeit2 = time.time()
        MaxZeit2 = StartZeit2 + self.Messung_Max
        # warte aus steigende Flanke von ECHO
        while StartZeit2 < MaxZeit2 and GPIO.input(self.US2_SENSOR_ECHO) == 0:
            StartZeit2 = time.time()

        # speichere Stopzeit
        StopZeit2 = StartZeit2
        # warte aus fallende Flanke von ECHO
        while StopZeit2 < MaxZeit2 and GPIO.input(self.US2_SENSOR_ECHO) == 1:
            StopZeit2 = time.time()

        if StopZeit2 < MaxZeit2:
            # berechne Zeitdifferenz zwischen Start und Ankunft in Sekunden
            Zeit2 = StopZeit2 - StartZeit2
            # berechne Distanz
            Distanz2 = Zeit2 * self.Messung_Faktor
        else:
            # setze Fehlerwert
            Distanz2 = self.Abstand_Max_Error

        # Distanz als Ganzzahl zurückgeben
        return int(Distanz2)

    def didStart(self):
        return self.didStart

    def getData(self) -> Union[UltrasonicData, None]:
        if (self.didStart == False):
            print(str("{} {} >> not configured".format(
                self.__class__, self.name)))

        else:
            distance1 = self.US_SENSOR1_GetDistance()
            distance2 = self.US_SENSOR2_GetDistance()

            if distance1 >= self.Abstand_Max:
                print("Kein Objekt gefunden")
            else:
                print("Gemessene Entfernung von Sensor 1= %i mm" % distance1)

            if distance2 >= self.Abstand_Max:
                print("Kein Objekt gefunden")
            else:
                print("Gemessene Entfernung von Sensor 2= %i mm \n\n" % distance2)

            return UltrasonicData(distance1, distance2)
