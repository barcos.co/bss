##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

# https://docs.circuitpython.org/projects/lsm303-accel/en/latest/

import board
import rospy
import adafruit_lsm303_accel
import adafruit_lsm303dlh_mag


from adafruit_lsm303_accel import Mode, Range, Rate
from adafruit_lsm303dlh_mag import MAGRATE_3_0, MAGGAIN_1_3
from typing import Union
from dataclasses import dataclass


@dataclass
class AccelerometerData:
    Ax: float = 0.0
    """ Beschleunigung in x in m/s^2 """
    Ay: float = 0.0
    """ Beschleunigung in y in m/s^2 """
    Az: float = 0.0
    """ Beschleunigung in z in m/s^2 """


@dataclass
class MagnetometerData:
    Mx: float = 0.0
    """ Magnetfeld in x in uT (micro Tesla) """
    My: float = 0.0
    """ Magnetfeld in y in uT (micro Tesla) """
    Mz: float = 0.0
    """ Magnetfeld in z in uT (micro Tesla) """


class Accelerometer:
    name = "LSM303"
    sensor = None
    didStart = False
    nodeName = ""

    def __init__(self, nodeName=""):
        self.nodeName = nodeName
        try:
            i2c = board.I2C()
            self.sensor = adafruit_lsm303_accel.LSM303_Accel(i2c)
            self.sensor.mode = Mode.MODE_HIGH_RESOLUTION
            # https://www.digikey.de/en/articles/what-you-need-to-know-to-choose-an-accelerometer
            self.sensor.range = Range.RANGE_8G
            self.sensor.data_rate = Rate.RATE_25_HZ
            self.didStart = True
        except Exception as err:
            self.didStart = False
            rospy.logerr('{} {} Exception: {}'.format(
                self.nodeName, self.name,  err))

    def didStart(self):
        return self.didStart

    def getData(self) -> Union[AccelerometerData, None]:
        if (self.didStart == False):
            rospy.logerr('{} {} I2C not configured'.format(
                self.nodeName, self.name))

        else:
            current = self.sensor.acceleration
            return AccelerometerData(current[0], current[1], current[2])


class Magnetometer:
    name = "LSM303DLH"
    sensor = None
    didStart = False
    nodeName = ""

    def __init__(self, nodeName=""):
        self.nodeName = nodeName
        try:
            i2c = board.I2C()
            self.sensor = adafruit_lsm303dlh_mag.LSM303DLH_Mag(i2c)
            self.sensor.mag_rate = MAGRATE_3_0  # 3Hz
            self.sensor.mag_gain = MAGGAIN_1_3
            self.didStart = True
        except Exception as err:
            self.didStart = False
            rospy.logerr('{} {} Exception: {}'.format(
                self.nodeName, self.name,  err))

    def didStart(self):
        return self.didStart

    def getData(self) -> Union[MagnetometerData, None]:
        if (self.didStart == False):
            rospy.logerr('{} {} I2C not configured'.format(
                self.nodeName, self.name))
        else:
            current = self.sensor.magnetic
            return MagnetometerData(current[0], current[1], current[2])
