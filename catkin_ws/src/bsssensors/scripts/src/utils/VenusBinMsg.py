##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##


import serial
from typing import Union

ACKID = int(0x83)
NACKID = int(0x84)


SetFactoryDefaultsID = 0x04

ConfigureSerialPortID = 0x05

SetPositionUpdateRateID = 0x0E
GetQueryPositionUpdateRateID = 0x10
ResponsePositionUpdateRate = 0x86


class VenusComands:
    def __init__(self) -> None:
        pass

    @staticmethod
    def getFactoryDefaultsID():
        return SetFactoryDefaultsID

    @staticmethod
    def setFactoryDefaults() -> bytes:
        requesttype = 0x00
        cw = [0xA0, 0xA1, 0x00, 0x02, VenusComands.getFactoryDefaultsID(),
              requesttype, 0x04, 0x0D, 0x0A]
        return serial.to_bytes(cw)

    @staticmethod
    def getQueryPositionUpdateRateID():
        return GetQueryPositionUpdateRateID

    @staticmethod
    def getQueryPositionUpdateRate() -> bytes:
        cw = [0xA0, 0xA1, 0x00, 0x01, VenusComands.getQueryPositionUpdateRateID(),
              0x10, 0x0D, 0x0A]
        return serial.to_bytes(cw)

    @staticmethod
    def setPositionUpdateRateID():
        return SetPositionUpdateRateID

    @staticmethod
    def getUpdateRate():
        fixRate = 0x0A
        # 10Hz could be 1, 2, 4, 5, 8, 10 or 20,
        # value with 4 ~10 should work with
        # baud rate 38400 or higher, value with 20
        # should work with baud rae 115200
        atributes = 0x00
        cw = [0xA0, 0xA1, 0x00, 0x03, VenusComands.setPositionUpdateRateID(
        ), fixRate, atributes, 0x0F, 0x0D, 0x0A]
        return serial.to_bytes(cw)

    @staticmethod
    def getConfigureSerialPortID():
        return ConfigureSerialPortID

    @staticmethod
    def getBaudrate():
        baudRate = 0x03  # 57600
        atributes = 0x00
        cw = [0xA0, 0xA1, 0x00, 0x04, VenusComands.getConfigureSerialPortID(
        ), 0x00, baudRate, atributes, 0x05, 0x0D, 0x0A]
        return serial.to_bytes(cw)

    def forceRestart(self):
        # 00 = Reserved
        # 01 = System Reset, Hot start
        # 02 = System Reset, Warm start
        # 03 = System Reset, Cold start
        # 04 = Reserved
        startMode = 0x01

        # UTC Year
        # now = datetime.datetime.now()
        yearField1 = 0x07
        yearField1 = 0xE6
        month = 0x12
        day = 0x09
        # min = hex(now.minute)
        # min = hex(now.minute)

        self.serialInst.write(
            bytearray([0xA0, 0xA1, 0x00, 0x0F, 0x01, startMode, yearField1, yearField1, month, day,
                       0x08, 0x2E, 0x03, 0x09, 0xC4, 0x30, 0x70, 0x00, 0x64, 0x16, 0x0D, 0x0A]))


class VenusBinMsg:
    msg = None

    def __init__(self, msg: Union[int, None]):
        self.msg = msg

    # iside ACK or NACK
    def getRequestID(self) -> Union[int, None]:
        result = None
        if self.isACK() or self.isNACK():
            # RequestID is always at the 6 place
            result = self.msg[5]
        return result

    def getMessageID(self) -> Union[int, None]:
        return self.getResponseID()

    # type of the message sent by the sensor
    def getResponseID(self) -> Union[int, None]:
        result = None
        if self.msg:
            # MessageID is always at the 5 place
            result = self.msg[4]
        return result

    def isACK(self) -> bool:
        return self.msg and ACKID == self.getResponseID()

    def isNACK(self) -> bool:
        return self.msg and NACKID == self.getResponseID()

    def isNACK(self) -> bool:
        return self.msg and NACKID == self.getResponseID()

    def isPositionRate(self) -> bool:
        return self.msg and ResponsePositionUpdateRate == self.getResponseID()

    def isConfigureBaudrate(self) -> bool:
        return self.msg and ConfigureSerialPortID == self.getResponseID()

    def getPositionUpdateRate(self) -> Union[int, None]:
        result = None
        if self.msg:
            result = self.msg[5]
        return result
