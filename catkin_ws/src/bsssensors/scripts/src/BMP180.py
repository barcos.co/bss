##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import time
import rospy
from typing import Union
from dataclasses import dataclass
import Adafruit_BMP.BMP085 as BMP085


@dataclass
class PressureAndTemperatureData:
    pressure: float = 0.0
    """ Druck in Pa """
    temperature: int = 0
    """ Temperatur in °C """
    altitude: float = 0.0
    """ Hohe in m """


class PressureAndTemperature:
    name = "BMP180"
    sensor = None
    didStart = False
    nodeName = ""

    def __init__(self, nodeName=""):
        self.nodeName = nodeName
        try:
            self.sensor = BMP085.BMP085()
            self.didStart = True
        except Exception as err:
            self.didStart = False
            rospy.logerr('{} {} Exception: {}'.format(
                self.nodeName, self.name,  err))

    def didStart(self):
        return self.didStart

    def getData(self) -> Union[PressureAndTemperatureData, None]:
        if (self.didStart == False):
            rospy.logerr('{} {} I2C not configured'.format(
                self.nodeName, self.name))

        else:
            temperature = self.sensor.read_temperature()
            pressure = self.sensor.read_pressure()
            altitude = self.sensor.read_altitude()
            return PressureAndTemperatureData(pressure, temperature, altitude)
