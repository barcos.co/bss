##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

# See more under https://github.com/Knio/pynmea2/blob/master/test/test_types.py

import io
import random
import pynmea2
import serial
import rospy
import datetime
from typing import Union

from dataclasses import dataclass
from src.utils.VenusBinMsg import VenusBinMsg, VenusComands


@dataclass
class Point:
    latitude: float = 0.0
    """ Abstand sensor 1 in mm """
    longitude: float = 0.0
    """ Abstand sensor 2 in mm """
    hasValue: bool = False
    velociy: float = 0.0


# rosrun bsssensors gnss.py


class GPS:
    name = "Venus638FLPx"
    cb = None
    pub = None
    nodeName = ""
    withRawMessage = False
    # serialInst = serial.Serial(port='/dev/serial0', baudrate=9600, timeout=1.0)
    # 4800 9600 19200 38400 115200
    serialInst = serial.Serial(
        port='/dev/ttyAMA0', baudrate=9600, timeout=5.0)

    def __init__(self, pub=None, nodeName=""):
        self.pub = pub
        self.nodeName = nodeName
        # self.setPositionUpdateRate()
        # self.resetToFactoryDefaults()

    def subscribe(self, cb):
        self.cb = cb

    def showRawMessage(self, show=True):
        self.withRawMessage = show

    def trackPosition(self):
        try:
            if self.serialInst.in_waiting:
                packet = self.serialInst.readline()

                if self.withRawMessage:
                    rospy.loginfo('{} {} paket: {}'.format(
                        self.nodeName, self.name,  packet))

                decoded = str(packet.decode("utf-8"))
                msg = pynmea2.parse(decoded)
                if msg:

                    # rospy.logwarn(msg.render())
                    # print(msg.__dict__)
                    # print(dir(msg))

                    # if msg.spd_over_grnd_kmph:
                    #     rospy.logwarn('{} {} {}, msg={}'.format(
                    #         self.nodeName, msg.talker, msg.spd_over_grnd_kmph, msg))

                    # if msg.sentence_type == 'GGA':
                    #     rospy.logwarn('{} gsp_signal={} | num_sats={}'.format(self.nodeName, msg.gps_qual, msg.num_sats))

                    # GPRMC = Recommended minimum specific GPS/Transit data
                    # Reading the GPGGA (fix data) is an alternative approach that also works
                    if msg.sentence_type == "RMC":
                        msg = pynmea2.RMC(
                            msg.talker, msg.sentence_type, msg.data)
                        hasPoint = True
                        # See more https://github.com/Knio/pynmea2/blob/43064671a955c5c8a0f950c9bc6b6ac2516d8868/pynmea2/types/talker.py
                        # See more http://cdn.sparkfun.com/datasheets/Sensors/GPS/Venus/638/doc/Venus638FLPx_DS_v07.pdf
                        rospy.loginfo('{} {} timestamp={} status={} latitude={} longitude={} spd_over_grnd={} true_course={} datestamp={}'.format(
                            self.nodeName, self.name, msg.timestamp, msg.status, msg.latitude, msg.longitude, msg.spd_over_grnd, msg.true_course, msg.datestamp))

                        self.cb(self.pub, Point(latitude=msg.latitude,
                                longitude=msg.longitude, hasValue=hasPoint), msg.spd_over_grnd, msg.true_course)

        except serial.SerialException as e:
            rospy.logerr('{} {} SerialException: {}'.format(
                self.nodeName, self.name,  e))
        except pynmea2.ParseError as e:
            rospy.logerr('{} {} ParseError:      {}'.format(
                self.nodeName, self.name, e))

    def readSerial(self):
        try:
            packet = self.serialInst.readline()
            if packet:
                self.evalResponse(packet)
                return packet
        except serial.SerialException as e:
            rospy.logerr('{} {} SerialException: {}'.format(
                self.nodeName, self.name,  e))

    def evalResponse(self, msg: Union[int, None]):
        venusPacket = VenusBinMsg(msg)

        # Eval ACK or NACK
        if venusPacket.isACK() or venusPacket.isNACK():
            reqId = venusPacket.getRequestID()
            resId = venusPacket.getResponseID()
            rospy.logwarn('{} {} READ::  ResponseID= {} RequestID= {}  packet= {}'.format(
                self.nodeName, self.name,  hex(resId), hex(reqId), msg))
        elif venusPacket.isPositionRate():
            rospy.logwarn('{} {} Position update rate = {}'.format(
                self.nodeName, self.name, venusPacket.getPositionUpdateRate()))
        elif venusPacket.isConfigureBaudrate():
            rospy.logwarn('{} {} Configure baud rate'.format(
                self.nodeName, self.name))
        # Eval other read types
        else:
            mesId = venusPacket.getMessageID()
            rospy.loginfo('{} {} READ::  MessageID= {}  packet= {}'.format(
                self.nodeName, self.name, hex(mesId), msg))

    def resetToFactoryDefaults(self):
        self.serialInst.write(VenusComands.setFactoryDefaults())
        rospy.logwarn('{} {} WRITE:: Factory reset comand sent. RequestID={}'.format(
            self.nodeName, self.name, hex(VenusComands.getFactoryDefaultsID())))

    def queryPositionUpdateRate(self):
        self.serialInst.write(VenusComands.getQueryPositionUpdateRate())
        rospy.logwarn('{} {} WRITE:: Query position update rate sent. RequestID={}'.format(
            self.nodeName, self.name, hex(VenusComands.getQueryPositionUpdateRateID())))

    def setPositionUpdateRate(self):
        # See http://dlnmh9ip6v2uc.cloudfront.net/datasheets/Sensors/GPS/Venus/638/doc/AN0003_v1.4.19.pdf
        # baudrate needs also to be updated. (on the GPS module and this object)
        self.serialInst.write(VenusComands.getUpdateRate())
        rospy.logwarn('{} {} WRITE:: Configure position update rate sent. RequestID={}'.format(
            self.nodeName, self.name, hex(VenusComands.setPositionUpdateRateID())))
        self.setBaudRate()
        self.serialInst.baudrate = 9600

    def setBaudRate(self):
        # See http://dlnmh9ip6v2uc.cloudfront.net/datasheets/Sensors/GPS/Venus/638/doc/AN0003_v1.4.19.pdf
        self.serialInst.write(VenusComands.getBaudrate())
        rospy.logwarn('{} {} WRITE:: Configure baudrate sent. RequestID={}'.format(
            self.nodeName, self.name, hex(VenusComands.getConfigureSerialPortID())))
