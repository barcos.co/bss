#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import uuid
import rospy

from bssmessage.msg import BSSACCPayload
from src.LSM303DLHC import Accelerometer


nodeName = "bsssensorsacc"
printNodeName = "[{}]".format(nodeName)
topicName = "bssdata/acc"

accel = Accelerometer(nodeName=nodeName)


def mainNode():
    # use a bigger queue_size than the rospy.Rate in order to prevent data loss
    pub = rospy.Publisher(topicName, BSSACCPayload, queue_size=200)
    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    # publishes at a rate of 25 messages per second
    rate = rospy.Rate(25)  # 25hz

    while not rospy.is_shutdown():

        msg = BSSACCPayload()

        msg.id = uuid.uuid4().hex
        msg.timestamp = rospy.get_rostime()
        aData = accel.getData()

        if aData:
            msg.Ax = aData.Ax
            msg.Ay = aData.Ay
            msg.Az = aData.Az

        rospy.loginfo(
            "{} BSSACCPayload will be published".format(printNodeName))
        rospy.loginfo("{} BSSACCPayload id={} time={}".format(
            printNodeName, msg.id, msg.timestamp))
        rospy.loginfo(
            "{} BSSACCPayload id={} published successfully\n".format(printNodeName, msg.id))

        pub.publish(msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        mainNode()
    except rospy.ROSInterruptException:
        pass
