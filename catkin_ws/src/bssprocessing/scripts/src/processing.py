#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import rospy
import math
import pandas as pd
import numpy as np
from statistics import stdev
from collections import deque

np.random.BitGenerator = np.random.bit_generator.BitGenerator
from bssmessage.msg import BSSNetConn, BSSACCPayload, BSSEddyPOSTPayload, BSSActuatorsPayload

QUEUE_MAX_SIZE = 50
class DataProcessor:
    hasInternet = False
    actuatorsPub = None
    eddyPub = None
    buffer = []

    smallest_std = 0.0


    queue = deque(maxlen=QUEUE_MAX_SIZE)
    std_queue = deque(maxlen=QUEUE_MAX_SIZE)

    def __init__(self) -> None:
        pass

    # moving algorithm
    def enqueue(self, data: BSSACCPayload):
        self.queue.append(data)
        if len(self.queue) == QUEUE_MAX_SIZE:
            self.procces(math.ceil(QUEUE_MAX_SIZE/ 2))

    
    def procces(self, index):
        (std, category) = self.calc_std()
        # element = self.queue[index]
        msg = BSSActuatorsPayload()
        msg.intensity = self.get_intensity(category)
        msg.triggeredBy = "processing"
        msg.type = str(category)
        self.actuatorsPub.publish(msg)

    
    def calc_std(self):
        
        # Extract the values of the "az" property from each element in the queue
        # az_values = [element["Az"] for element in self.queue]
        az_values = []
        for element in list(self.queue):
            az_values.append(element.Az)


        local_std = stdev(az_values)
        self.std_queue.append(local_std)
        category  = 0 # unclissified



        if len(self.std_queue) == QUEUE_MAX_SIZE:
            # Use numpy.histogram() to create 5 categories
            hist, bins = np.histogram(self.std_queue, bins=5)
            category = np.digitize(local_std, bins)


        if local_std == 0:
            self.smallest_std = local_std
        elif local_std < self.smallest_std:
            self.smallest_std = local_std
        
        return (local_std, category)
    
    def get_intensity(self, category:int):
        if category == 1:
            return 0
        elif category <=2:
            return category * 15
        elif category <= 4:
            return category *15
        else:
            return max(category * 15, 100)

