
from dataclasses import dataclass
from typing import List, Any, Union
import geographiclib.geodesic as geo
from bssmessage.msg import BSSWayPoint, BSSEddyGETPayload


@dataclass
class ActuatorCallback:
    topic: str
    callback: Any


@dataclass
class NearestPoint:
    type: str
    latitude: float
    longitude: float
    distance: float


class Radar():
    nearestPoint: Union[NearestPoint, None] = None

    currentLat = 0.0
    currentLong = 0.0
    currentSpeed = 0.0
    currentCourse = 0.0

    isMoving = False
    direction = 0.0
    innerPolygon = []
    outerPolygon = []
    currentWayPoints: List[BSSWayPoint] = []

    pub: Any
    actuatorscb: List[ActuatorCallback] = []

    def __init__(self):
        print("Radar initiated")

    def registerPublisher(self, pub):
        self.pub = pub

    def registerActuatorCallback(self, topic: str, callback):
        self.actuatorscb.append(ActuatorCallback(
            topic=topic, callback=callback))

    def update(self, data: BSSEddyGETPayload):
        self.isMoving = data.isMoving
        self.direction = data.direction
        self.innerPolygon = data.innerPolygon
        self.outerPolygon = data.outerPolygon
        self.currentWayPoints = data.wayPoints

    # scan find the nearest point to the current posistion inside the current way points

    def findNearestPoint(self) -> Union[NearestPoint, None]:

        minDist = float('inf')
        if len(self.currentWayPoints):
            for point in self.currentWayPoints:
                type = point.type
                latitude = point.latitude
                longitude = point.longitude

                distance = geo.Geodesic.WGS84.Inverse(
                    self.currentLat, self.currentLong, latitude, longitude)['s12']

                if distance < minDist:
                    minDist = distance
                    self.nearestPoint = NearestPoint(
                        type=type, latitude=latitude, longitude=longitude, distance=distance)

        return self.nearestPoint

    # The actuators are notified in a scala from 0 to 100.  A higher intensity means the
    # point is too close to the current location.
    def notifyActuators(self):
        if self.nearestPoint:
            intensity = self.calcIntensity()
            for cb in self.actuatorscb:
                cb.callback(self.pub, cb.topic, intensity=intensity,
                            triggeredBy="radar", type=self.nearestPoint.type)

    # algorithm in python for calculating a intensity based on distance of the nearest point.
    # distances between 1m and 12m should have an scaled intensity between 100 and 10.
    # distances bigger than 12m has an intensity of 0
    #
    # The first case, checks if the distance is less than 1m, in that case the intensity is set to 100.
    # The second case, checks if the distance is less or equal than 12m, in that case we use the
    # following formula: intensity = 100 - (distance - 1) * (90/11)  Where we subtract from 100 the product
    # of the difference between the distance and 1 by the ratio 90/11. This will give us a value between
    # 90 and 10, as the distance increases from 1 to 12m.
    def calcIntensity(self):
        nullIntensity = 0
        minDistance = 1
        maxDistance = 12

        if self.nearestPoint:
            distance = self.nearestPoint.distance
            if distance <= minDistance:
                return 100
            elif distance <= maxDistance:
                # return 100 - (distance - 1) * (90/11)
                return 100 - (distance - minDistance) * (90/(maxDistance - minDistance))
            # elif distance < 3:
            #     return 90
            # elif distance < 6:
            #     return 80
            # elif distance < 12:
            #     return 80 - (distance - 6) * (70/6)
            else:
                return nullIntensity
        return nullIntensity
