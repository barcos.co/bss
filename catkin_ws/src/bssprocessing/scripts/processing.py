#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import uuid
import socket
import rospy

from bssmessage.msg import BSSNetConn, BSSACCPayload, BSSEddyPOSTPayload, BSSActuatorsPayload
from src.processing import DataProcessor
nodeName = "bssproccessinproccessing"
printNodeName = "[{}]".format(nodeName)
netTopicName = "bsscore/net"
accTopicName = "bssdata/acc"
eddyPostTopicName = "bsshttp/eddy/post"
defActuatorsTopicName = "bssactuators/default"

# imuTopicName = "bssdata/imu"
# gnssTopicName = "bssdata/gnss"
# ussTopicName = "bssdata/uss"

processor = DataProcessor()


def updateNetConn(data: BSSNetConn):
    if processor.hasInternet != data.isConnected:
        processor.hasInternet = data.isConnected
        rospy.logwarn("{} updateNetConn isConnected={}".format(
        printNodeName, data.isConnected))

def handleAcc(data: BSSACCPayload):
    processor.enqueue(data)

def mainNode():

    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    processor.actuatorsPub = rospy.Publisher(defActuatorsTopicName, BSSActuatorsPayload, queue_size=20)
    processor.eddyPub = rospy.Publisher(eddyPostTopicName, BSSEddyPOSTPayload, queue_size=20)

    rospy.Subscriber(netTopicName, BSSNetConn, updateNetConn)
    rospy.Subscriber(accTopicName, BSSACCPayload, handleAcc)

    rospy.spin()


if __name__ == '__main__':
    try:
        mainNode()
    except rospy.ROSInterruptException:
        pass
