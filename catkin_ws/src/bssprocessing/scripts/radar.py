#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import rospy

from bssmessage.msg import BSSEddyGETPayload, BSSGNSSPayload, BSSActuatorsPayload
from src.radar import Radar

nodeName = "bssproccessingradar"
printNodeName = "[{}]".format(nodeName)

gnssTopicName = "bssdata/gnss"
eddyGetTopicName = "bsshttp/eddy/get"
defActuatorsTopicName = "bssactuators/default"


radar = Radar()


def updateGNSSData(data: BSSGNSSPayload):
    rospy.loginfo("{} updateGNSSData course={} lat={} long={} speed={}".format(
        printNodeName, data.course, data.latitude, data.longitude, data.speed))
    radar.currentLat = data.latitude
    radar.currentLong = data.longitude
    radar.currentSpeed = data.speed
    radar.currentCourse = data.course


def handlerEddyGET(payload: BSSEddyGETPayload):
    rospy.loginfo("{} handlerEddyGET".format(printNodeName))
    radar.update(payload)


def notifyActuators(pub, topic: str, intensity: int, triggeredBy: str, type: str):
    msg = BSSActuatorsPayload()
    msg.intensity = intensity
    msg.triggeredBy = triggeredBy
    msg.type = type

    rospy.loginfo(
        "{} BSSActuatorsPayload will be published to {}".format(printNodeName, topic))
    pub.publish(msg)
    rospy.loginfo(
        "{} BSSActuatorsPayload published successfully\n".format(printNodeName, msg.id))


def mainNode():
    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    rospy.Subscriber(gnssTopicName, BSSGNSSPayload, updateGNSSData)
    rospy.Subscriber(eddyGetTopicName, BSSEddyGETPayload, handlerEddyGET)

    pub = rospy.Publisher(defActuatorsTopicName,
                          BSSActuatorsPayload, queue_size=20)

    radar.registerPublisher(pub)
    radar.registerActuatorCallback(defActuatorsTopicName, notifyActuators)

    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        radar.findNearestPoint()
        radar.notifyActuators()
        rate.sleep()


if __name__ == '__main__':
    try:
        mainNode()
    except rospy.ROSInterruptException:
        pass
