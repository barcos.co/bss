#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import uuid
import socket
import rospy

from bssmessage.msg import BSSNetConn

nodeName = "bsscorenet"
printNodeName = "[{}]".format(nodeName)
topicName = "bsscore/net"

# Snapshot from https://github.com/mfoc/monitor-internet-connection
# The project was published under the MIT License


def is_internet_alive(host="8.8.8.8", port=53, timeout=3) -> bool:
    """Check if Internet Connection is alive and external IP address is reachable.
    Input Parameters:
        host: (string) 8.8.8.8 (google-public-dns-a.google.com)
        port: (integer) (53/tcp DNS Service).
        timeout: (float) timeout in seconds.
    Returns:
        True (Boolean) if external IP address is reachable.
        False (Boolean) if external IP address is unreachable.
    """

    try:
        socket.setdefaulttimeout(timeout)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
    except OSError as error:
        rospy.logdebug("{} BSSNetConn error: ".format(
            printNodeName, error.strerror))
        return False
    else:
        s.close()
        return True


def mainNode():
    pub = rospy.Publisher(topicName, BSSNetConn, queue_size=10)
    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    # publishes at a rate of 1 message per second
    rate = rospy.Rate(1)  # 1hz

    while not rospy.is_shutdown():
        msg = BSSNetConn()

        msg.id = uuid.uuid4().hex
        msg.timestamp = rospy.get_rostime()
        msg.isConnected = is_internet_alive()

        rospy.loginfo("{} BSSNetConn will be published".format(printNodeName))
        rospy.loginfo("{} BSSNetConn id={} time={} isConnected={}".format(
            printNodeName, msg.id, msg.timestamp, msg.isConnected))
        rospy.loginfo(
            "{} BSSNetConn id={} published successfully\n".format(printNodeName, msg.id))

        pub.publish(msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        mainNode()
    except rospy.ROSInterruptException:
        pass
