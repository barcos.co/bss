#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import uuid
import rospy

from bssmessage.msg import BSSI2CState

nodeName = "bsscorei2c"
printNodeName = "[{}]".format(nodeName)
topicName = "bsscore/i2c"


def isI2CBusActive() -> bool:
    # rospy.logwarn(
    #     "{} BSSI2CState isI2CBusActive needs to be implemented".format(printNodeName))
    return True


def getI2CDeviceCount() -> int:
    # rospy.logwarn(
    #     "{} BSSI2CState getI2CDeviceCount needs to be implemented".format(printNodeName))
    return 8


def mainNode():
    pub = rospy.Publisher(topicName, BSSI2CState, queue_size=10)
    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    # publishes at a rate of 1 message per second
    rate = rospy.Rate(1)  # 1hz

    while not rospy.is_shutdown():
        msg = BSSI2CState()

        msg.id = uuid.uuid4().hex
        msg.timestamp = rospy.get_rostime()
        msg.isBusActive = isI2CBusActive()
        msg.deviceCount = getI2CDeviceCount()

        rospy.loginfo("{} BSSI2CState will be published".format(printNodeName))
        rospy.loginfo("{} BSSI2CState id={} time={} isBusActive={} deviceCount={}".format(
            printNodeName, msg.id, msg.timestamp, msg.isBusActive, msg.deviceCount))
        rospy.loginfo(
            "{} BSSI2CState id={} published successfully\n".format(printNodeName, msg.id))

        pub.publish(msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        mainNode()
    except rospy.ROSInterruptException:
        pass
