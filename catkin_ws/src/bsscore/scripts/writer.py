#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import os
import csv
import uuid
import rospy

from datetime import datetime
from dataclasses import dataclass
from bssmessage.msg import BSSIMUPayload, BSSUSSPayload, BSSGNSSPayload, BSSACCPayload


nodeName = "bsscorewriter"
printNodeName = "[{}]".format(nodeName)

# Note the RasperryOS for the Raspberry Pi 4 Model B @ 2018
# uses ext4 for the main Partition.
# Run sudo lsblk -o NAME,FSTYPE,SIZE,MOUNTPOINT for more info
#
# The use of ext4 means that the max. number of files is 4 billion
# This property is tracked globally and not in each directory.
# The number of subdirectories is unlimited while the

# Write in folder ${project_root}/data
# Write imu to ${project_root}/data/imu/ddmmyy-[count].csv
# Write uss to ${project_root}/data/uss/ddmmyy-[count].csv
# Write gnss to ${project_root}/data/gnss/ddmmyy-[count].csv


class Writer:
    startTime = None
    basepath = ""
    completeFolder = ""
    header = None

    def __init__(self, folderName: str, header) -> None:
        self.basepath = os.environ.get("WRITER_PATH", "/root/catkin_ws/data")
        self.startTime = datetime.now()
        self.header = header
        self.config(folderName)
        pass

    def config(self, folderName: str):
        self.createFolder(folderName)

    def createFolder(self, folderName: str):
        self.completeFolder = self.basepath + "/" + folderName
        exists = os.path.exists(self.completeFolder)
        if not exists:
            os.makedirs(self.completeFolder)
            rospy.loginfo("{} Writer folder {} created successfully".format(
                printNodeName, self.completeFolder))

    def write(self, payload):
        fileName = self.getFileName()
        if self.shouldChangeFile() or not os.path.exists(fileName):
            self.ensureHeader()

        with open(fileName, "a", encoding="UTF8") as someFile:
            someWriter = csv.DictWriter(someFile, fieldnames=self.header)
            someWriter.writerow(payload)

    def ensureHeader(self):
        fileName = self.getFileName()
        with open(fileName, "w", encoding="UTF8") as someFile:
            someWriter = csv.DictWriter(someFile, fieldnames=self.header)
            someWriter.writeheader()

    def getFileName(self):
        year = self.startTime.year
        month = self.startTime.month
        day = self.startTime.day
        fileName = '{:02d}{:02d}{:02d}.csv'.format(year, month, day)
        return "{}/{}".format(self.completeFolder, fileName)

    def shouldChangeFile(self) -> bool:
        if datetime.now().day != self.startTime.day:
            self.startTime = datetime.now()
            return True

        return False


class Sensor:
    writer = None
    topicName: str = ""
    msgClass = None

    def __init__(self, type: str, msgClass, header) -> None:
        self.writer = Writer(type, header)
        self.msgClass = msgClass
        self.topicName = "bssdata/" + type
        pass

    @staticmethod
    def getBaseProps(payload):
        return payload.id, payload.timestamp


class IMUSensor(Sensor):
    def __init__(self) -> None:
        super().__init__(type="imu", msgClass=BSSIMUPayload, header=["id", "timestamp",
                                                                     "pressure", "temperature", "altitude",
                                                                     "Wx", "Wy", "Wz",
                                                                     "Mx", "My", "Mz",
                                                                     "Ax", "Ay", "Az"])

    def cb(self, payload: BSSIMUPayload):
        id, timestamp = super().getBaseProps(payload)

        # Pressure (Pa), Temp (°C) and altitude (m)
        pressure = payload.pressure
        temperature = payload.temperature
        altitude = payload.altitude

        # GyroscopeData in rad/s
        Wx = payload.Wx
        Wy = payload.Wy
        Wz = payload.Wz

        # MagnetometerData in uT (micro Tesla)
        Mx = payload.Mx
        My = payload.My
        Mz = payload.Mz

        # AccelerometerData in m/s^2
        Ax = payload.Ax
        Ay = payload.Ay
        Az = payload.Az

        self.writer.write(
            payload={"id": id, "timestamp": timestamp,
                     "pressure": pressure, "temperature": temperature, "altitude": altitude,
                     "Wx": Wx, "Wy": Wy, "Wz": Wz,
                     "Mx": Mx, "My": My, "Mz": Mz,
                     "Ax": Ax, "Ay": Ay, "Az": Az
                     })
        rospy.loginfo(
            "{} BSSIMUPayload written id={}".format(printNodeName, id))

class ACCSensor(Sensor):
    def __init__(self) -> None:
        super().__init__(type="acc", msgClass=BSSACCPayload, header=["id", "timestamp",
                                                                     "Ax", "Ay", "Az"])

    def cb(self, payload: BSSACCPayload):
        id, timestamp = super().getBaseProps(payload)

        # AccelerometerData in m/s^2
        Ax = payload.Ax
        Ay = payload.Ay
        Az = payload.Az

        self.writer.write(
            payload={"id": id, "timestamp": timestamp,
                     "Ax": Ax, "Ay": Ay, "Az": Az
                     })
        rospy.loginfo(
            "{} BSSACCPayload written id={}".format(printNodeName, id))

class USSSensor(Sensor):
    def __init__(self) -> None:
        super().__init__(type="uss", msgClass=BSSUSSPayload,
                         header=["id", "timestamp", "distance1", "distance2"])

    def cb(self, payload: BSSUSSPayload):
        id, timestamp = super().getBaseProps(payload)
        distance1 = payload.distance1
        distance2 = payload.distance2
        self.writer.write(
            payload={"id": id, "timestamp": timestamp, "distance1": distance1, "distance2": distance2})
        rospy.loginfo(
            "{} BSSUSSPayload written id={}".format(printNodeName, id))


class GNSSSensor(Sensor):
    def __init__(self) -> None:
        super().__init__(type="gnss", msgClass=BSSGNSSPayload, header=[
            "id", "timestamp", "hasValue", "latitude", "longitude", "speed", "course"])

    def cb(self, payload: BSSGNSSPayload):
        id, timestamp = super().getBaseProps(payload)
        hasValue = payload.hasValue
        latitude = payload.latitude
        longitude = payload.longitude
        speed = payload.speed
        course = payload.course
        self.writer.write(
            payload={"id": id, "timestamp": timestamp, "hasValue": hasValue, "latitude": latitude, "longitude": longitude, "speed": speed, "course": course})
        rospy.loginfo(
            "{} BSSGNSSPayload written id={}".format(printNodeName, id))


def writterNode():
    rospy.init_node(nodeName, anonymous=False)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    sensoren = [ACCSensor(), IMUSensor(), USSSensor(), GNSSSensor()]

    for sensor in sensoren:
        rospy.Subscriber(sensor.topicName, sensor.msgClass, sensor.cb)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    try:
        writterNode()
    except rospy.ROSInterruptException:
        pass
