##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

from dataclasses import dataclass


@dataclass
class Properties:
    createdAt: str = ""
    createdBy: str = ""
    type: str = ""
    group: str = "",
    users: str = "",
    deviceId: str = ""


@dataclass
class Geometry:
    type: str = ""
    latitude: float = 0.0
    longitude: float = 0.0


@dataclass
class GeoReference:
    id: str = ""
    type: str = ""
    properties: Properties = Properties()
    geometry: Geometry = Geometry()
