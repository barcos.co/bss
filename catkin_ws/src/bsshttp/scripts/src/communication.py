##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

# ApiCommunication Class for capsuling the code


import json
import rospy
import requests
import concurrent.futures
from typing import TypeVar, Generic, Union, Any, List, Dict

T = TypeVar("T")


class APICommunication(Generic[T]):
    apiKey = ""
    apiSecret = ""

    # defaltAccept = "text/plain"
    defaultContentType = "application/geo+json"

    def __init__(self, apiKey: str, apiSecret: str = ""):
        self.apiKey = apiKey
        self.apiSecret = apiSecret

    def printValues(self):
        rospy.logwarn("[APICommunication]:: API Key = " + self.apiKey)
        rospy.logwarn("[APICommunication]:: API Secret = " + self.apiSecret)

    def get(self, url: str) -> Union[Dict[str, Any], None]:
        headers = self.getHeaders()
        response = requests.get(url, headers=headers)
        if response.status_code == 200 or response.status_code == 201:
            data = response.json()
            rospy.loginfo("[APICommunication]:: Method: GET  | Success")
            return data
        else:
            rospy.logerr(
                "[APICommunication]:: Method: GET  | Error: " + response.reason)
            return None

    def post(self, url: str, payload) -> str:
        headers = self.getHeaders()
        rospy.loginfo(
            "[APICommunication]:: Method: POST | INFO: posting to url " + url)
        # rospy.loginfo(json.dumps(payload, indent=4))
        # rospy.loginfo("[APICommunication]:: posting to url " + payload)

        response = requests.post(url=url, json=payload, headers=headers)
        return self.handleResponse(response=response)

    def postInParallel(self, url: str, payloads) -> List[str]:
        ids = []
        headers = self.getHeaders()
        with concurrent.futures.ThreadPoolExecutor() as executor:
            results = [executor.submit(
                requests.post, url, json=payload, headers=headers) for payload in payloads]
            for f in concurrent.futures.as_completed(results):
                response = f.result()
                localId = self.handleResponse(response=response)
                if len(localId):
                    ids.append(localId)

        return ids

    def handleResponse(self, response: requests.Response) -> str:
        id = ""
        if response.status_code == 200 or response.status_code == 201:
            response_header = dict(response.headers)
            # rospy.loginfo(json.dumps(response_header, indent=4))
            location = response_header['Location']
            object_id = location.split("/")[-1]
            id = object_id
            rospy.loginfo(
                "[APICommunication]:: Method: POST | Success: data sent to the server")
            rospy.loginfo(
                "[APICommunication]:: Method: POST | Success: created object_id " + object_id)
        else:
            rospy.logerr(
                "[APICommunication]:: Method: POST | Error: while sending data to the server")
            rospy.logerr("[APICommunication]:: Error: " + response.reason)

        return id

    def getHeaders(self):
        headers = {
            "Content-Type": self.defaultContentType,
            "apiKey": self.apiKey
        }

        return headers
