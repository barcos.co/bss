##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import rospy
import json
from typing import Union, List, Any, Dict
from bssmessage.msg import BSSWayPoint, BSSLatLong
from geographiclib.geodesic import Geodesic
from .communication import APICommunication
from .eddy_geo_reference import GeoReference, Geometry, Properties


class EddyServer:
    collection_id = "traffic_signs"
    server_url = "https://eddy01.comloc.net/oaf/collections/"
    conn = APICommunication[GeoReference]("haw-team5-d410f8dadb82")
    group = ""
    hasInternet = False
    currentCourse = 0.0
    currentLat = 0.0
    currentLong = 0.0
    currentSpeed = 0.0
    currentWayPoints: List[BSSWayPoint] = []

    def __init__(self, group: int):
        self.conn.printValues()
        self.group = str(group)

    def getURL(self, itemId=""):
        base = self.server_url + self.collection_id + "/items"
        if len(itemId) > 0:
            base = base + "/" + itemId

        return base + "?apiKey=" + self.conn.apiKey + "&createdBy=haw-team" + self.group

    def getGeoReferenceById(self, id: str) -> Union[GeoReference, None]:
        url = self.getURL(itemId=id)
        ref = self.conn.get(url)

        if ref != None:
            properties = self.getProperties(ref["properties"])
            geometry = self.getGeometry(ref["geometry"])
            return GeoReference(id=ref["id"], type=ref["type"], properties=properties, geometry=geometry)
        else:
            return None

    def storeGeoReferences(self, type: str, points: List[BSSWayPoint], deviceId: str) -> List[str]:
        payloads = []

        for point in points:
            payloads.append({
                "type": "Feature",
                "geometry": {"type": "Point", "coordinates": [point.latitude, point.longitude]},
                "properties": {
                    "type": type,
                    "group": self.group,
                    "users": "haw-hamburg",
                    "deviceId": deviceId,
                }
            })

        url = self.getURL()
        return self.conn.postInParallel(url=url, payloads=payloads)

    def getGeometry(self, raw: Dict[str, Any]) -> Geometry:
        type = raw.get("type")
        coordinates = raw.get("coordinates")
        return Geometry(type=type, latitude=coordinates[0], longitude=coordinates[1])

    def getProperties(self, raw: Dict[str, Any]) -> Properties:
        createdBy = raw.get("createdBy")
        createdAt = raw.get("createdAt")
        type = raw.get("type")
        group = raw.get("group")
        users = raw.get("users")
        deviceId = raw.get("deviceId")

        return Properties(
            createdAt=createdAt,
            createdBy=createdBy,
            type=type,
            group=group,
            users=users,
            deviceId=deviceId
        )

    # fetchNaerestPoints fetch the important points inside a given box.
    # its keep the local eddy server object up to date with information
    # about the found points in the remote server
    def fetchNaerestPoints(self, box:  List[BSSLatLong]):
        foundWayPoints = []

        try:
            limit = "&limit=" + str(1000)
            sw = str(box[0].latitude) + "%2C" + str(box[0].latitude)
            ne = str(box[0].latitude) + "%2C" + str(box[0].latitude)
            bbox = "&bbox=" + sw + "%2C" + ne

            url = self.getURL() + limit + bbox
            jsonBody = self.conn.get(url)

            if len(jsonBody):
                numberMatched = jsonBody.get("numberMatched")
                numberReturned = jsonBody.get("numberReturned")
                if numberReturned:

                    rospy.loginfo(
                        "[EddyServer]:: fetchNaerestPoints=" + str(numberReturned) + " of numberMatched=" + str(numberMatched))
                    # rospy.loginfo(json.dumps(jsonBody, indent=4))
                    points: List[Dict[str, Any]] = jsonBody.get("features")
                    for point in points:
                        geometry = self.getGeometry(point.get("geometry"))
                        properties = self.getProperties(
                            point.get("properties"))
                        foundWayPoint = BSSWayPoint(
                            type=properties.type, latitude=geometry.latitude, longitude=geometry.longitude)
                        foundWayPoints.append(foundWayPoint)

                    self.currentWayPoints = foundWayPoints
                else:
                    rospy.loginfo(
                        '[EddyServer]:: fetchNaerestPoints= {}'.format(numberReturned))
        except Exception as err:
            rospy.logerr('[EddyServer]:: Exception: {}'.format(err))

    def getBoundingBox(self, center: BSSLatLong, distanceFromCenter: float) -> List[BSSLatLong]:
        neAzimuth = 45
        swAzimuth = 225

        gne = Geodesic.WGS84.Direct(
            center.latitude, center.longitude, neAzimuth, distanceFromCenter)

        gsw = Geodesic.WGS84.Direct(
            center.latitude, center.longitude, swAzimuth, distanceFromCenter)

        pointSW = BSSLatLong(latitude=gsw['lat2'], longitude=gsw['lon2'])
        pointNe = BSSLatLong(latitude=gne['lat2'], longitude=gne['lon2'])

        return [pointSW, pointNe]

    def getCurrentCenter(self) -> BSSLatLong:
        return BSSLatLong(latitude=self.currentLat, longitude=self.currentLong)

    # getInnerPolygon create the boundingbox small [3m from center point] arround the current location.
    # def getInnerPolygon(self, currentLat: float, currentLong: float, course: float) -> List[BSSLatLong]:
    def getInnerPolygon(self):

        bbox = self.getBoundingBox(
            center=self.getCurrentCenter(), distanceFromCenter=3)

        # TODO v2 get the boundingbox with a custom course
        # point = BSSLatLong()
        # point.latitude = 0.0
        # point.longitude = 0.0

        # innerPolygon = []
        # innerPolygon.append(point)
        # return innerPolygon
        return bbox

    # getInnerPolygon create the boundingbox small [10m from center point] arround the current location.
    # def getOuterPolygon(self, currentLat: float, currentLong: float, course: float) -> List[BSSLatLong]:
    def getOuterPolygon(self):
        bbox = self.getBoundingBox(
            center=self.getCurrentCenter(), distanceFromCenter=10)

        # TODO v2 get the boundingbox with a custom course
        # point = BSSLatLong()
        # point.latitude = 0.0
        # point.longitude = 0.0

        # outerPolygon = []
        # outerPolygon.append(point)
        # return outerPolygon
        return bbox
