#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import rospy
from src.eddy import EddyServer
from bssmessage.msg import BSSNetConn, BSSGNSSPayload, BSSEddyPOSTPayload, BSSEddyGETPayload

nodeName = "bsshttpeddy"
printNodeName = "[{}]".format(nodeName)
netTopicName = "bsscore/net"
gnssTopicName = "bssdata/gnss"
eddyPostTopicName = "bsshttp/eddy/post"
eddyGetTopicName = "bsshttp/eddy/get"

# gruppe 5 im Fach BMT6-Sensorik
# WiSe 2022/23
# deviceId="prototype-1.0"
eddyServer = EddyServer(group=5)


def updateNetConn(data: BSSNetConn):
    if eddyServer.hasInternet != data.isConnected:
        eddyServer.hasInternet = data.isConnected
        rospy.logwarn("{} updateNetConn isConnected={}".format(
        printNodeName, data.isConnected))


def updateGNSSData(data: BSSGNSSPayload):
    rospy.loginfo("{} updateGNSSData course={} lat={} long={} speed={}".format(
        printNodeName, data.course, data.latitude, data.longitude, data.speed))
    eddyServer.currentLat = data.latitude
    eddyServer.currentLong = data.longitude
    eddyServer.currentSpeed = data.speed
    eddyServer.currentCourse = data.course


def handleEddyPOST(data: BSSEddyPOSTPayload):
    if eddyServer.hasInternet:
        rospy.loginfo("{} handleEddyPOST type={} #of_wayPoints={} deviceId={}".format(
            printNodeName, data.type, len(data.wayPoints), data.deviceId))
        ids = eddyServer.storeGeoReferences(
            type=data.type, points=data.wayPoints, deviceId=data.deviceId)
        rospy.loginfo("{} handleEddyPOST success. created wayPoints={}".format(
            printNodeName, len(ids)))
    else:
        rospy.logerr("{} handleEddyPOST type={} #of_wayPoints={} deviceId={} not posible. Verify your network connection".format(
            printNodeName, data.type, len(data.wayPoints), data.deviceId))


def mainNode():

    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    rospy.Subscriber(netTopicName, BSSNetConn, updateNetConn)
    rospy.Subscriber(gnssTopicName, BSSGNSSPayload, updateGNSSData)
    rospy.Subscriber(eddyPostTopicName, BSSEddyPOSTPayload, handleEddyPOST)

    pub = rospy.Publisher(eddyGetTopicName, BSSEddyGETPayload, queue_size=10)

    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    # publishes at a rate of 1 message per 2 seconds
    rate = rospy.Rate(0.5)  # 0.5hz

    while not rospy.is_shutdown():

        if eddyServer.hasInternet:
            msg = BSSEddyGETPayload()
            msg.direction = eddyServer.currentCourse
            # speed 2 prevents positive negatives
            msg.isMoving = eddyServer.currentSpeed > 2

            msg.innerPolygon = eddyServer.getInnerPolygon()
            msg.outerPolygon = eddyServer.getOuterPolygon()

            box = msg.innerPolygon
            if msg.isMoving:
                box = msg.outerPolygon

            eddyServer.fetchNaerestPoints(box)

            msg.wayPoints = eddyServer.currentWayPoints
            pub.publish(msg)
            rospy.loginfo(
                "{} Successfully published the current radar information".format(printNodeName))
        else:
            rospy.logerr(
                "{} the current radar information wasnt published. Verify your network connection".format(printNodeName))

        rate.sleep()


if __name__ == '__main__':
    try:
        mainNode()
    except rospy.ROSInterruptException:
        pass
