#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

# Adapted example from https://gpiozero.readthedocs.io/en/stable/api_output.html?highlight=buzzer#tonalbuzzer

import rospy
import math
from time import sleep
from typing import Union
from threading import Thread, Event
from gpiozero import TonalBuzzer
from gpiozero.tones import Tone
from dataclasses import dataclass

MAX_FREQ = 880.0
MIN_FREQ = 220.0
DEFAULT_GPIO_PORT = 22


@dataclass
class MelodyTone:
    duration: int = 0
    note: str = ""


class Buzzer():
    tonalBuzzer = TonalBuzzer(DEFAULT_GPIO_PORT)
    nodeName: str = ""
    currentIntensity = 0

    isBeeping = False

    thread: Union[Thread, None] = None
    stopEvent = Event()

    def __init__(self, nodeName: str):
        self.nodeName = nodeName

        # Start the beep function in the background using a thread
        self.thread = Thread(target=self.watchIntensity)
        self.thread.start()
        self.tonalBuzzer.play(MAX_FREQ - 200)
        sleep(0.3)
        self.tonalBuzzer.stop()

    def watchIntensity(self):
        while True:
            rep = self.calcRepetitionInterval()
            if self.currentIntensity > 0:
                self.tonalBuzzer.play(MAX_FREQ -200)
                self.isBeeping = True
                sleep(0.1)
                self.tonalBuzzer.stop()
            else:
                self.tonalBuzzer.stop()
            sleep(rep)

    def setIntensity(self, intensity: int):
        self.currentIntensity = intensity

    def stopBeep(self):
        self.currentIntensity = 0
        self.stopEvent.set()
        self.thread.join()
        self.tonalBuzzer.stop()

    def calcRepetitionInterval(self):
        # If intensity is 100, go sleep 0.1s then reproduce 10 beeps each second or 600bpm
        if self.currentIntensity >= 100:
            return 0.1
        # If intensity is o then go sleep 5secs
        elif self.currentIntensity <= 0:
            return 5
        else:
            return 5 - (5 * (1 - math.exp(-self.currentIntensity / 25)))

    def playMelody(self, melody: str, frequency: float, duration: float):
        try:
            if not melody:
                freq = frequency

                if frequency >= MAX_FREQ:
                    rospy.logwarn("{} BSSBuzzerPayload frequency={} too high. Adjusting to ${}".format(
                        self.nodeName, frequency, MAX_FREQ))
                    freq = MAX_FREQ

                if frequency <= MIN_FREQ:
                    rospy.logwarn("{} BSSBuzzerPayload frequency={} too low. Adjusting to ${}".format(
                        self.nodeName, frequency, MIN_FREQ))
                    freq = MIN_FREQ

                self.tonalBuzzer.play(Tone(freq))
                rospy.sleep(duration)
                self.tonalBuzzer.stop()
            else:
                rospy.logwerr("{} BSSBuzzerPayload melody={} not supported".format(
                    self.nodeName, melody))

        except Exception as e:
            rospy.logerr(
                "{} BSSBuzzerPayload Exception :: {}".format(self.nodeName, e))


# melody = [
#     MelodyTone(note="0",duration=4),
#     MelodyTone(note="E4",duration=8),
#     MelodyTone(note="E4",duration=8),
#     MelodyTone(note="A3",duration=4),
#     MelodyTone(note="E4",duration=4),
#     MelodyTone(note="200",duration=4),
#     MelodyTone(note="B3",duration=4),
#     MelodyTone(note="C4",duration=4)]

# for note in melody:
#     try:
#         pasiveBuzzer.play(note.note)
#         rospy.sleep(note.duration)
#     except Exception as e:
#         rospy.logerr("{} BSSBuzzerPayload Exception :: {}".format(printNodeName, e))
#         continue

# Other Melody
# t = 0
# notes = [262, 294, 330, 262, 262, 294, 330, 262, 330, 349, 392, 330, 349, 392, 392,
#          440, 392, 349, 330, 262, 392, 440, 392, 349, 330, 262, 262, 196, 262, 262, 196, 262]
# duration = [2, 0.1, 2, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 1, 0.5, 0.5, 1, 0.25, 0.25,
#             0.25, 0.25, 0.5, 0.5, 0.25, 0.25, 0.25, 0.25, 0.5, 0.5, 0.5, 0.5, 1, 0.5, 0.5, 1]
# for n in notes:
#     pasiveBuzzer.play(n)
#     # buzz(n, duration[t])
#     rospy.sleep(duration[t] * 0.1)
#     t += 1

# spin() simply keeps python from exiting until this node is stopped
