#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import rospy
from gpiozero import RGBLED
from colorzero import Color
from bssmessage.msg import BSSRGBLedPayload


DEFAULT_RED_GPIO_PORT = 13
DEFAULT_GREEN_GPIO_PORT = 19
DEFAULT_BLUE_GPIO_PORT = 26
MAX_COLOR_CHANGE_TIMEOUT = 5


class RGBLed():
    color = Color('green')
    led = RGBLED(DEFAULT_RED_GPIO_PORT, DEFAULT_GREEN_GPIO_PORT,
                 DEFAULT_BLUE_GPIO_PORT)

    def __init__(self, nodeName: str) -> None:
        self.printNodeName = nodeName
        self.setIsMoving(4)
        rospy.sleep(3)
        self.setIsMoving(0)
        self.led.color = self.color

    def setIsMoving(self, speed: float):
        isMoving: bool = False

        # Needed minimun offset for preventing False-Positives speeds
        if speed > 2:
            isMoving = True

        if isMoving:
            self.led.blink(on_time=0.1, off_time=0.1, on_color=self.color)
        else:
            self.led.color = self.color

    def handleIntensity(self, intensity:float):

        if intensity >=40 and intensity< 70:
            self.color = Color("yellow")
            self.led.color = self.color
            return

        if intensity >= 70:
            self.color = Color("red")
            self.led.color = self.color
            return
        else:
            self.color = Color("green")
            self.led.color = self.color
            return
  


    def handleCustomState(self, data: BSSRGBLedPayload):
        changeColor = "green"

        try:
            if not data.state:

                # Change the color if color present
                if len(data.color) != 0:
                    changeColor = data.color

                # Change the color if rgb present
                if data.red or data.green or data.blue:
                    someColor = Color.from_rgb(
                        r=data.red, g=data.green, b=data.blue)
                    if someColor:
                        changeColor = someColor

                self.led.color = Color(changeColor)
                rospy.sleep(MAX_COLOR_CHANGE_TIMEOUT)

            else:
                # state triggered by the system
                # One of "RUNNING", "PARKED", "ALARM", "INTERNAL_ERROR"
                changeColor = "green"
                if data.state == "RUNNING":
                    changeColor = "green"
                elif data.state == "PARKED":
                    changeColor = "yellow"
                elif data.state == "ALARM":
                    changeColor = "red"
                elif data.state == "INTERNAL_ERROR":
                    changeColor = "red"
                else:
                    changeColor = "green"

                self.led.color = Color(changeColor)

        except Exception as e:
            rospy.logerr(
                "{} BSSRGBLedPayload Exception :: {}".format(self.printNodeName, e))
