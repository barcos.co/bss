#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

# Adapted example from https://gpiozero.readthedocs.io/en/stable/api_output.html#rgbled
# More under https://www.youtube.com/watch?v=ioEcJhX3sdo

import rospy
from src.rgbled import RGBLed
from bssmessage.msg import BSSRGBLedPayload, BSSActuatorsPayload, BSSGNSSPayload


nodeName = "bssactuatorsrgbled"
printNodeName = "[{}]".format(nodeName)
topicName = "bssactuators/rgbled"
defTopicName = "bssactuators/default"
gnssTopicName = "bssdata/gnss"

rgbLED = RGBLed(nodeName=printNodeName)


def updateGNSSData(data: BSSGNSSPayload):
    rospy.loginfo("{} updateGNSSData course={} lat={} long={} speed={}".format(
        printNodeName, data.course, data.latitude, data.longitude, data.speed))
    rgbLED.setIsMoving(data.speed)


def defCallback(data: BSSActuatorsPayload):
    rospy.loginfo("{} BSSActuatorsPayload arrived triggeredBy={} type={} intensity={}".format(
        printNodeName, data.triggeredBy, data.type, data.intensity))
    rgbLED.handleIntensity(intensity=data.intensity)


def callback(data: BSSRGBLedPayload):
    rospy.loginfo("{} BSSRGBLedPayload arrived id={} time={} r={} g={} b={} state={} color={}".format(
        printNodeName, data.id, data.timestamp, data.red, data.green, data.blue, data.state, data.color))
    rgbLED.handleCustomState(data=data)


def mainListener():

    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    rospy.Subscriber(defTopicName, BSSActuatorsPayload, defCallback)
    rospy.Subscriber(topicName, BSSRGBLedPayload, callback)
    rospy.Subscriber(gnssTopicName, BSSGNSSPayload, updateGNSSData)

    rospy.spin()


rospy.on_shutdown(rgbLED.led.off)

if __name__ == '__main__':
    try:
        mainListener()
    except rospy.ROSInterruptException:
        pass
