#!/usr/bin/env python3

##
# HAW Hamburg WiSe 2022/23
#
# BSS Projekt im Studienfach BMT6 Sensorik - Gruppe 5
# bei Prof. Dr. Rasmus Rettig
#
# Erstellt durch:
# Helmer Luis Barcos Paso
# HelmerLuis.BarcosPaso@haw-hamburg.de
# helmer@barcos.co
# https://barcos.co
##

import rospy
from bssmessage.msg import BSSBuzzerPayload
from bssmessage.msg import BSSActuatorsPayload
from src.buzzer import Buzzer


nodeName = "bssactuatorsbuzzer"
printNodeName = "[{}]".format(nodeName)
topicName = "bssactuators/buzzer"
defTopicName = "bssactuators/default"

pasiveBuzzer = Buzzer(printNodeName)


def defCallback(data: BSSActuatorsPayload):
    rospy.loginfo("{} BSSActuatorsPayload arrived triggeredBy={} type={} intensity={}".format(
        printNodeName, data.triggeredBy, data.type, data.intensity))
    pasiveBuzzer.setIntensity(data.intensity)


def callback(data: BSSBuzzerPayload):
    rospy.loginfo("{} BSSBuzzerPayload arrived id={} time={} frequency={}".format(
        printNodeName, data.id, data.timestamp, data.frequency))
    pasiveBuzzer.playMelody(
        melody=data.melody, frequency=data.frequency, duration=data.duration)


def mainListener():

    rospy.init_node(nodeName, anonymous=True)
    rospy.loginfo("{} Did start successfully".format(printNodeName))

    rospy.Subscriber(defTopicName, BSSActuatorsPayload, defCallback)
    rospy.Subscriber(topicName, BSSBuzzerPayload, callback)

    rospy.spin()


if __name__ == '__main__':
    try:
        mainListener()
    except rospy.ROSInterruptException:
        pass
