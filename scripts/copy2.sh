#! /bin/sh

FIRST_PARAM=$1

USER=pi
REMOTE_HOST="${FIRST_PARAM:-bmt6-se-gruppe6.local}"
REMOTE_PATH=/bmt6-sensorik-gruppe6

OMIT=catkin_ws/data
DOC2=docker-compose.yaml
DOC3=Dockerfile
DOC4=Dockerfile.test

rsync --exclude=$OMIT -r catkin_ws $DOC2 $DOC3 $DOC4 $USER@$REMOTE_HOST:$REMOTE_PATH