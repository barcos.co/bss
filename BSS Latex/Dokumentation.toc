\babel@toc {ngerman}{}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}%
\contentsline {subsection}{\numberline {1.1}Ziel des Projektes}{1}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Bereitgestellte Hardware}{2}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Organisation und Zeitplanung}{3}{subsection.1.3}%
\contentsline {section}{\numberline {2}Anforderungen}{4}{section.2}%
\contentsline {section}{\numberline {3}Konzeption und Theorie}{6}{section.3}%
\contentsline {subsection}{\numberline {3.1}Verwendete Software}{6}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Ubuntu}{6}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}ROS}{6}{subsubsection.3.1.2}%
\contentsline {subsection}{\numberline {3.2}Verwendete Hardware}{8}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Microcontroller}{8}{subsubsection.3.2.1}%
\contentsline {subsection}{\numberline {3.3}Verwendete Sensorik}{10}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}Inertiale Messeinheit (IMU)}{10}{subsubsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.2}GNSS-Sensor}{12}{subsubsection.3.3.2}%
\contentsline {subsection}{\numberline {3.4}Verwendete Aktorik}{13}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Komponentenübersicht}{14}{subsection.3.5}%
\contentsline {section}{\numberline {4}Implementierung}{16}{section.4}%
\contentsline {subsection}{\numberline {4.1}Installation der Software}{16}{subsection.4.1}%
\contentsline {subsubsection}{\numberline {4.1.1}Remote-Zugriff}{16}{subsubsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.2}Docker}{17}{subsubsection.4.1.2}%
\contentsline {subsubsection}{\numberline {4.1.3}Installation ROS}{19}{subsubsection.4.1.3}%
\contentsline {subsection}{\numberline {4.2}Software-Architektur}{20}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}BSS core nodes: writer, i2c und net}{23}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}BSS sensors nodes: gnss, imu und acc}{23}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}BSS actuators nodes: rgbled und buzzer}{24}{subsubsection.4.2.3}%
\contentsline {subsubsection}{\numberline {4.2.4}BSS HTTP nodes: eddy}{24}{subsubsection.4.2.4}%
\contentsline {subsubsection}{\numberline {4.2.5}BSS processing nodes: processing und radar}{25}{subsubsection.4.2.5}%
\contentsline {subsubsection}{\numberline {4.2.6}BSS Core Topics: \textit {/bsscore/*}}{25}{subsubsection.4.2.6}%
\contentsline {subsubsection}{\numberline {4.2.7}BSS Data Topics: \textit {/bssdata/*}}{26}{subsubsection.4.2.7}%
\contentsline {subsubsection}{\numberline {4.2.8}BSS Actuators Topics: \textit {/bssactuators/*}}{28}{subsubsection.4.2.8}%
\contentsline {subsubsection}{\numberline {4.2.9}BSS HTTP Topics: \textit {/bsshttp/*}}{29}{subsubsection.4.2.9}%
\contentsline {subsection}{\numberline {4.3}Konstruktion Gehäuse}{29}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}Rapid Prototyping}{30}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Modellierung des Gehäuses}{31}{subsubsection.4.3.2}%
\contentsline {subsection}{\numberline {4.4}Systemaufbau}{34}{subsection.4.4}%
\contentsline {subsubsection}{\numberline {4.4.1}Verkabelung}{34}{subsubsection.4.4.1}%
\contentsline {subsubsection}{\numberline {4.4.2}Montage}{35}{subsubsection.4.4.2}%
\contentsline {subsection}{\numberline {4.5}Daten sammeln}{37}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Datenauswertung}{37}{subsection.4.6}%
\contentsline {section}{\numberline {5}Tests}{42}{section.5}%
\contentsline {subsection}{\numberline {5.1}Abschlusstest}{42}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Validierung}{45}{subsection.5.2}%
\contentsline {section}{\numberline {6}Fazit}{46}{section.6}%
\contentsline {section}{\numberline {A}Stückliste}{47}{appendix.A}%
\contentsline {section}{\numberline {B}Technische Zeichungen des Gehäuses}{48}{appendix.B}%
\contentsline {subsection}{\numberline {B.1}Akkukasten }{48}{subsection.B.1}%
\contentsline {subsection}{\numberline {B.2}Elektronikkasten }{49}{subsection.B.2}%
\contentsline {subsection}{\numberline {B.3}Deckel }{50}{subsection.B.3}%
\contentsline {subsection}{\numberline {B.4}Dichtung Akkukasten }{51}{subsection.B.4}%
\contentsline {subsection}{\numberline {B.5}Dichtung Elektronikkasten }{52}{subsection.B.5}%
\contentsline {subsection}{\numberline {B.6}Explosionszeichnung mit Stückliste }{53}{subsection.B.6}%
\contentsline {section}{\numberline {C}Nodes}{54}{appendix.C}%
\contentsline {subsection}{\numberline {C.1}BSS core nodes}{54}{subsection.C.1}%
\contentsline {subsubsection}{\numberline {C.1.1}writer.py}{54}{subsubsection.C.1.1}%
\contentsline {subsubsection}{\numberline {C.1.2}i2c.py}{59}{subsubsection.C.1.2}%
\contentsline {subsubsection}{\numberline {C.1.3}net.py}{61}{subsubsection.C.1.3}%
\contentsline {subsection}{\numberline {C.2}BSS sensors nodes}{63}{subsection.C.2}%
\contentsline {subsubsection}{\numberline {C.2.1}gnss.py}{63}{subsubsection.C.2.1}%
\contentsline {subsubsection}{\numberline {C.2.2}imu.py}{65}{subsubsection.C.2.2}%
\contentsline {subsubsection}{\numberline {C.2.3}acc.py}{71}{subsubsection.C.2.3}%
\contentsline {subsection}{\numberline {C.3}BSS actuators nodes}{74}{subsection.C.3}%
\contentsline {subsubsection}{\numberline {C.3.1}buzzer.py}{74}{subsubsection.C.3.1}%
\contentsline {subsubsection}{\numberline {C.3.2}rgbled.py}{79}{subsubsection.C.3.2}%
\contentsline {subsection}{\numberline {C.4}BSS http nodes}{83}{subsection.C.4}%
\contentsline {subsubsection}{\numberline {C.4.1}eddy.py}{83}{subsubsection.C.4.1}%
\contentsline {subsection}{\numberline {C.5}BSS processing nodes}{92}{subsection.C.5}%
\contentsline {subsubsection}{\numberline {C.5.1}processing.py}{92}{subsubsection.C.5.1}%
\contentsline {subsubsection}{\numberline {C.5.2}radar.py}{96}{subsubsection.C.5.2}%
\contentsline {section}{\numberline {D}Messages}{98}{appendix.D}%
\contentsline {subsection}{\numberline {D.1}BSSACCPayload.msg}{98}{subsection.D.1}%
\contentsline {subsection}{\numberline {D.2}BSSActuatorsPayload.msg}{98}{subsection.D.2}%
\contentsline {subsection}{\numberline {D.3}BSSBuzzerPayload.msg}{98}{subsection.D.3}%
\contentsline {subsection}{\numberline {D.4}BSSEddyGETPayload.msg}{98}{subsection.D.4}%
\contentsline {subsection}{\numberline {D.5}BSSEddyPOSTPayload.msg}{99}{subsection.D.5}%
\contentsline {subsection}{\numberline {D.6}BSSGNSSPayload.msg}{99}{subsection.D.6}%
\contentsline {subsection}{\numberline {D.7}BSSI2CState.msg}{99}{subsection.D.7}%
\contentsline {subsection}{\numberline {D.8}BSSIMUPayload.msg}{100}{subsection.D.8}%
\contentsline {subsection}{\numberline {D.9}BSSLatLong.msg}{100}{subsection.D.9}%
\contentsline {subsection}{\numberline {D.10}BSSNetConn.msg}{100}{subsection.D.10}%
\contentsline {subsection}{\numberline {D.11}BSSPoint.msg}{101}{subsection.D.11}%
\contentsline {subsection}{\numberline {D.12}BSSRGBLEDPayload.msg}{101}{subsection.D.12}%
\contentsline {subsection}{\numberline {D.13}BSSWayPoint.msg}{101}{subsection.D.13}%
\contentsline {section}{\numberline {E}Docker}{102}{appendix.E}%
\contentsline {subsection}{\numberline {E.1}Dockerfile}{102}{subsection.E.1}%
\contentsline {subsection}{\numberline {E.2}docker-compose.yaml}{104}{subsection.E.2}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
