# BMT6-Sensorik / Bicycle Sensor System (BSS) 🤖 🚴

HAW Hamburg ROS Project für das Fach BMT6 Sensorik in WiSe 2022/23. The Bicycle Sensor System attempts to identify dangerous situations for cyclists.
See more in the documentation file (german version)

<br>
<div align="center" justify="center">
<img src="system_architecture/BSS_circuit_diagram.png" align="center" width="100%" alt="Realtime Chat">
</div>
<br>

This Project will need:

- Docker and Docker Compose
- Python 3.9 (Automatically installed via docker)
- The 10 DOF Adafruit Module
- The SparkFun Venus GPS with SMA Connector
- 1 Raspberry Pi 4 Model B - 2018 with 2GB of RAM
- A couple of Jumper Wire Kabels Female to Male
- A couple of Jumper Wire Kabels Male to Male
- A Steckboard
- Others Hardware for the current source (TBE)
- The case should be printed as 3D Print
- A set of Abstandhelter Male to Female M2.5
- Some Double-Sided Tape
- Others (TBE)

<br>
<div align="center" justify="center">
<img src="system_architecture/system_achritecture 2.0.png" align="center" width="100%" alt="Realtime Chat">
</div>
<br>

# PVL

- docker exec -it bssros /bin/bash
- cd to data/imu
- tail -fn +1 20221220.csv
- cd to data/gnss
- tail -fn +1 20221220.csv
-
- rostopic list
- rostopic echo /bsscore/net

# How to start

1. Install Raspberry Pi OS 64 Bits (Desktop Version) inside the SDCard. Name the host `bmt6-se-gruppe6.local`. User must be pi and password password
2. Edit the config file. and add Following lines at the bottom

   ```
   [all]

   # Config a Power button
   # WakeUp on falling edge on GPIO3
   # TurnOff on falling edge on GPIO17
   dtoverlay=gpio-shutdown,gpio_pin=17,active_low=1,gpio_pull=up

   # Disable the Bluetooth Module since it use the UART0/UART1
   dtoverlay=disable-bt

   # Enable UART1
   enable_uart=1

   # Adjust CPU Speed since UART1 make it slower
   force_turbo=1
   ```

3. Login to the Pi via ssh `ssh pi@bmt6-se-gruppe6.local` pwd = pasword

   - Activate the VNC Server by executing `sudo raspi-config` and following the instructions
   - Activate the I2C Bus by executing `sudo raspi-config` and following the instructions
   - Boot the raspberry pi `sudo reboot now`
   - Tets VNC Connection. Download the VNC Client from https://www.realvnc.com/en/connect/download/viewer/
     - Host=bmt6-se-gruppe6.local, user=pi, password=password
   - Test the I2C Interface `i2cdetect -y 1`. A matrix 00->07 and 0->f should be listed

4. Install Docker in Raspberry Pi https://docs.docker.com/engine/install/debian/
   - `sudo apt-get update`
   - `curl -fsSL https://get.Docker.com -o get-Docker.sh`
   - `sudo sh get-Docker.sh`
   - `sudo usermod -aG docker $USER`
5. Move the current content to the Raspberry Pi.
   - Create a folder inside the raspberrypi `sudo mkdir /bmt6-sensorik-gruppe6`
   - Grant permissions `sudo chmod 777 /bmt6-sensorik-gruppe6`
   - Move inside to folder `cd /bmt6-sensorik-gruppe6`
   - From outside the Raspberry Pi open a terminal session inside the current project
   - Execute `./scripts/copy2.sh`. This will copy all local files from the project to the raspberry pi. Note that rsync is needed here.
   - Confirm the password
6. Ensure Following sensors/actuators ares connected
   - TODO: List all sensors/actuators
7. Execute the test container in oder to make sure all Sensors are working
   - TODO: work in progress
   - In the Raspberry Pi execute `docker compose --profile test up`. Make sure you are inside `/bmt6-sensorik-gruppe6` before running this docker command
8. Exit the proccess, delete te container and delete the image.
   - `CTRL+C`. This will stop the container
   - `docker compose --profile test down --rmi -v`. This will remove the network and the container. Make sure you are inside `/bmt6-sensorik-gruppe6` before running this docker command
9. Run the real container `docker compose --profile ros up`. Make sure you are inside `/bmt6-sensorik-gruppe6` before running this docker command. Your nodes should be all up and running

## Bonus (in progress)

The Docker image will start a server on http://bmt6-se-gruppe6.local Here you will see a graphic WebUI from wich you could see your running nodes, topic, and other information and options like the possibility start or stop some nodes.

You could access from your pc or your smartphone. Keep in mind that the raspberry pi and your pc or smartphone need to be connected to the same network

- In order to swicth over networks your may actiavte the NetworkManager. https://pimylifeup.com/raspberry-pi-network-manager/. Keep in mind that current connections may be lose. A Ethernet cable is recommended.

## How To

### read the .csv file

- `tail -fn +1 20221210.csv` Keep in mind you must be cd into `/data/imu` folder. or oder under `data/gnns` or `data/uss`

### see the logs of the nodes

- Login to the container, `docker exec -it bssros /bin/bash`
- find the roslog folder `roslaunch-logs`
- move to that dir and list all files.
- log the file you want. For example. `tail -fn +1 /root/.ros/log/e5a83106-7855-11ed-8c09-0242ac120002/writer-4.log`

### create a package

- Be sure to be logged in to the Raspberry pi
- Start the Container `docker compose --profile ros up --build -d`. This will start the roscore in detached mode.
- Login to the container `docker exec -it bssros /bin/bash`
- Move to the `src` folder `cd src`
- Create the package `catkin_create_pkg some_pkg std_msgs roscpp rospy ..otherlibs` example: `catkin_create_pkg test_package std_msgs roscpp rospy`
- Build the worspace `catkin_make`. Make sure you execute this inside the `catkin_ws` folder
- Test your packages executables. A example was already created. You could execute the publisher by running `rosrun bssexample publisher.py` and the susbcriber by runnning `rosrun bssexample susbcriber.py`. You shoud want to login in diferent terminals in order to see it in action.

### create a Message

- Create package where the messages will be stored. For example a package called `bssmessage` with the previous instruccions
- Follow the following tutorial https://medium.com/@lavanyaratnabala/create-custom-message-in-ros-52664c65970d
- Import your custom messages and used them like shown inside publisher.py and subscriber.py of `bssexample/scripts`.
- Run your node. If you get the error `ModuleNotFoundError: No module named 'your-message-package'`. You should probably exit the Docker session and try to login again.
- List your message if neede `rosmsg show BSSPoint` or `rosmsg show BSSNetConn` or `rosmsg show YourMessageType`

# ROS

## ROS Basics

`roscore` is the master node : master (provides name service for ROS) + rosout (stdout/stderr) + parameter server

1. `rosnode list` : list command lists these active nodes. You will see: `/rosout`, since there is only one node running: rosout.
2. `rosnode info /rosout` : info command returns information about a specific node.
3. `rosrun [package_name] [node_name]` Runs a node from a given package. Allows you to use the package name to directly run a node within a package (without having to know the package path)
4. `rostopic list` : list all topics
5. `rostopic pub [/topic-name] [data-type]` publish some data of some type to some topic
6. `rqt_graph` for displaying the pub sub graph

## Create a package

1. `catkin_create_pkg package_name std_msgs rospy` creates a package
2. Source the bash file `source /catkin_ws/devel/setup.bash`
3. Finde the package `rospack find [package name]`
4. `catkin_make` inside catkin_ws folder

## Create a Node

1. Inside `catkin_ws/src/package_name/scripts` create you first node. `node1.py`
2. Git it execution permissions `chmod +X node1.py`
3. more under https://www.youtube.com/watch?v=jWtkzDbez9M

## Create Custom Msgs

- catkin_create_pkg custom_msg rospy message_generation message_runtime std_msgs geometry_msgs
- more unter https://www.youtube.com/watch?v=baAE0i8Rzvg
- ensure `source ~/.bashrc`

## Python local development

1. Press Ctrl+Shift+P in VS code and click on Select Interpreter.
2. Try to set the path that you got from your terminal as a python path

## Rostopic

- `rostopic list` list all active topics
- `rostopic echo /bsscore/net` list topic for the net status

# Docker

## Login to the ROS Docker Container

- `docker exec -it bssros /bin/bash` - Change the name of the container is you change the name ros1 inside the docker-compose.yml file
- `rosrun communication_pkg`

1. http://wiki.ros.org/ROS/Tutorials/CreatingPackage

See more https://hub.docker.com/r/osrf/ros

# Code Editor

## VS Code

1. Install python extension
2. Install CMake Extansion
3. Install ROS Extension

# Helpful links

- https://medium.com/swlh/7-simple-steps-to-create-and-build-our-first-ros-package-7e3080d36faa
- Creating symlink "/root/catkin_ws/src/CMakeLists.txt" pointing to "/opt/ros/noetic/share/catkin/cmake/toplevel.cmake"
- run `catkin_make` inside `/root/catkin_ws`
- https://answers.ros.org/question/9177/adding-a-dependency-after-roscreate-pkg/
- https://medium.com/swlh/part-3-create-your-first-ros-publisher-and-subscriber-nodes-2e833dea7598

# https://github.com/simonwu53/RoadSurfaceRecognition
